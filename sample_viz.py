import holoviews
import numpy as np
import holoviews as hv
from holoviews import opts
import sys
from typing import cast

hv.extension("plotly")


def get_n_nearest(data: np.ndarray, n=5):
    pass


# generate sample 3D data
rng = np.random.default_rng()
size = cast(int, int(sys.argv[1]))
fake_data = rng.standard_normal(3 * size).reshape(size, 3)
fake_data = fake_data[
    np.argsort(np.sum(fake_data, axis=1) ** 2), :
]  # sort by distance from origin
# determine which stars are
start, end = fake_data[[0, -1], :]

# plot
plot = hv.Scatter3D(fake_data).opts(
    opts.Scatter3D(cmap="fire", bgcolor="black", size=1)
)
hv.save(plot, filename="test.html")
